require('./db');
require('./server');
const MONGOOSE = require('mongoose');
const EXPRESS = require('express');
const BODYPARSER = require('body-parser');
var APP = EXPRESS();
APP.use(BODYPARSER.urlencoded({
    extended :true
}));

APP.use(BODYPARSER.json());
//code to add and request of society to get confirmed 
const SCHEMA = MONGOOSE.Schema;
let societyProfile = new SCHEMA({
    societyName :{type : String, required :true, maxlength : 25},
    societyAddress : {
        plotNo:{type : Number,required : true},
        street1 : { type : String, required :true},
        street2 : {type : String,},
        city : {type : String ,required : true},
        pinCode : {type : Number,required : true},
        State : {type :String,required : true},
        country :{type :String,required : true},        
    },
    date : {type : Date,required :false,default :Date.now},
    reraRegistrationNo : {type : Number,required : true},
    noOfFloors : {type : Number,required : true},
    noOfFlats : {type :Number,required :true }
});
var societyreqprofile = MONGOOSE.model('societyReqprofile',societyProfile,'societyReqprofile');

var addReq = function (req,res){
    var society = new societyreqprofile();
    society.societyName = req.body.societyName;
    society.societyAddress = req.body.societyAddress;
    society.reraRegistrationNo = req.body.reraRegistrationNo;
    society.noOfFloors = req.body.noOfFloors;
    society.noOfFlats = req.body.noOfFlats;
    society.save((err,rult)=>{
        if(err)
        {
            res.json("err in inserting society request record"+err);
        }else{
            res.json("record successfully inserted");
        }
    });
   }
   module.exports = {societyreqprofile,addReq}
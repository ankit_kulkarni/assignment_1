require('../societyprofilerequest/db');
const MONGOOSE = require('mongoose');
const EXPRESS = require('express');
const BODYPARSER = require('body-parser');
var APP = EXPRESS();
const req_to_profile_society_member = require("./app");
APP.use(BODYPARSER.urlencoded({
    extended :true
}));

APP.use(BODYPARSER.json());

function showReqProfile(req,res){
    req_to_profile_society_member.societyreqprofile.find({},(err,obj)=>{
        if(err){
            res.json("something went wrong");
        }else{
           res.json(obj);
        }
    })
}
module.exports = showReqProfile
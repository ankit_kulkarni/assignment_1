require('./server')
const JWT = require("jsonwebtoken");
const MONGOOSE = require("mongoose");
const EXPRESS = require("express");
const BODYPARSER = require("body-parser");
var APP =EXPRESS();
APP.use(BODYPARSER.json())
const SCHEMA = MONGOOSE.Schema;
var loginProfile = new SCHEMA({
    EmailID : {type:String,required : true},
    password : {type :String ,required :true}
}); 
loginProfile.path('EmailID').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');
var masterLogin = MONGOOSE.model('masterLogin',loginProfile,'masterLogin');

var fetchmaster = function (req,res){
   // console.log(req.body.EmailID);
    var query = { "EmailID" : req.body.EmailID };
    masterLogin.findOne(query)
    .then((data)=>{
        if(data!=null){
      //  console.log(data);
        if ( req.body.EmailID === data.EmailID && req.body.password === data.password )  {
          //  console.log("validation");
            let token = JWT.sign({ username: data.EmailID }, "thankyouiauro");
           res.json({
                token:token
            });
        }
        else {
            res.json("no valid data present");
        }
    }else{
        res.json("enter a valid data");
    }

     })
    .catch((err)=>{
      res.json(err);
    })    
}
module.exports = {fetchmaster}
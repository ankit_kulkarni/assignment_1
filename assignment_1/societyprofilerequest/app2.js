const JWT =require('jsonwebtoken');
require('./db');
const MONGOOSE = require('mongoose');
const EXPRESS = require('express');
const BODYPARSER = require('body-parser');
const SOCIETYREQPROFILE = require('./app');
var APP = EXPRESS();
APP.use(BODYPARSER.urlencoded({
  extended :true
}));
APP.use(BODYPARSER.json());

const SCHEMA = MONGOOSE.Schema;
var confirmProfile = new SCHEMA({
    societyName :{type : String, required :true, maxlength : 25},
    societyAddress : {
        plotNo:{type : Number,required : true},
        street1 : { type : String, required :true},
        street2 : {type : String,},
        city : {type : String ,required : true},
        pinCode : {type : Number,required : true},
        State : {type :String,required : true},
        country :{type :String,required : true,default : "india"},        
    },
    date : {type : Date,required :false,default :Date.now},
    reraRegistrationNo : {type : Number,required : true},
    noOfFloors : {type : Number,required : true},
    noOfFlats : {type :Number,required :true }
    });
var confirmSocietyProfile = MONGOOSE.model('confirmSocietyProfile',confirmProfile,'confirmSocietyProfile');
APP.post('/confirmSociety', ensureToken, (req, res) =>{
    JWT.verify(req.token, 'thankyouiauro', function(err, data) {
      if (err) {
        res.sendStatus(403);
        res.json("error in verifying");
      } else {
     res.json("verified");
     fetchFromReq(req);
      }
    });
  })
  function ensureToken(req, res, next) {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      req.token = bearerToken;
      next();
    } else {
      res.sendStatus(403);
    }
  }
    function fetchFromReq(req){
      var email = req.body.EmailID;
      console.log(email);
      var query = { "EmailID" : email };
      SOCIETYREQPROFILE.findOne(query)
      .then((data)=>{
        req.json("in confirmSocietyProfile");
       })
      .catch((err)=>{
       res.json(err);
      })    
    }
  function confirmSocietyProfile(){
            var society = new confirmSocietyProfile();
        society.societyName = data.societyName;
        society.societyAddress = data.Address;
        society.reraRegistrationNo = data.reraRegistrationNo;
        society.noOfFloors = data.noOfFloors;
        society.noOfFlats = data.noOfFlats;
        console.log("in confirmSocietyProfile");
        society.save(async(err,result)=>{
          if(err)
          {
            res.json("err in inserting society request record");
          }else{
            try{
              await SOCIETYREQPROFILE.deleteOne({ "Email" : req.body.Email })
            }catch(error){
              res.json("Error:",error);
          }
           res.json(result);
		  }
      });

   }
//code to delete record
function deletReqRecord(req){
   var email = req.body.EmailID;
            console.log("in delete record");
            var query = { "EmailID" : email };
            SOCIETYREQPROFILE.delete(query)
            .then((data)=>{
                res.json(data);
            })
            .catch((err)=>{
              res.json(err);
            });    
  }
  //code to add or profile in confirmed society
require('../societyprofilerequest/db');
const MONGOOSE = require("mongoose");
const EXPRESS = require("express");
const BODYPARSER = require("body-parser");
const APP =EXPRESS();
APP.use(BODYPARSER.json());

const login = require('./app1');
const addRequest = require('./app');
const middleware = require('./middleware')
const confirmMemberRequest = require('../societymembership/confirmMemberRequest');
const reqtoSocietyMembers = require('../societymembership/reqToSocietyMember');
const addmaintainance =require('../societymaintainance/add_maintainance');
const SocietyMaintainance = require('../societymaintainance/SocietyMaintainance');
const delay_maintainance = require('../payment/delay_maintainance');
const addMemberBySociety = require('../societymembership/addMemberBySociety');
const showReqProfile = require('./show_reqested_profile');
const showAcceptedProfile = require('./show_accepted_profile');
APP.listen(3000,()=>{
    console.log("server started");
   });
   
APP.get('/showAcceptedProfile',showAcceptedProfile);
APP.get('/showReqProfile',showReqProfile); //society req profile
APP.post('/login',login.fetchmaster);
APP.post('/addReqSociety',addRequest.addReq);
APP.post('/addMemberBySociety',middleware.checkToken,addMemberBySociety);
APP.post('/confirmMemberRequest',confirmMemberRequest.insertRecord);
APP.post('/reqtoSocietyMembers',reqtoSocietyMembers.insertrecord);
APP.put('/add_maintainance',addmaintainance.addMaintainance);
APP.get('/SocietyMaintainance',SocietyMaintainance);
APP.post('/delay_maintainance',delay_maintainance);
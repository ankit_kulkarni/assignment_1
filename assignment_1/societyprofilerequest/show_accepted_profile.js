require('../societyprofilerequest/db');
const MONGOOSE = require('mongoose');
const EXPRESS = require('express');
const BODYPARSER = require('body-parser');
var APP = EXPRESS();
const confirmMemberRequestSchema = require("../societymembership/confirmMemberRequest");
APP.use(BODYPARSER.urlencoded({
    extended :true
}));

APP.use(BODYPARSER.json());

function showAcceptedProfile(req,res){
    confirmMemberRequestSchema.User.find({},(err,obj)=>{
        if(err){
            res.json("something went wrong");
        }else{
           res.json(obj);
        }
    })
}
module.exports = showAcceptedProfile
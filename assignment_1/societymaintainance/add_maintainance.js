require('../societyprofilerequest/db');
const MONGOOSE = require('mongoose');
const EXPRESS = require('express');
const BODYPARSER = require('body-parser');
var APP = EXPRESS();
APP.use(BODYPARSER.urlencoded({
    extended :true
})); 

APP.use(BODYPARSER.json());
const SCHEMA = MONGOOSE.Schema;
let personal_maintainance_schema =new SCHEMA({
    flat_no:{type : Number,maxlength : 4},
    email : {type : String,required : true},
    maintainance : {type : Number},
    reason : {type :String},
    last_date_of_payment :{type : Date,required : true}
});
var User = MONGOOSE.model("flatno",personal_maintainance_schema,"flatno");
var addMaintainance = function(req,res){
    console.log(req.body);
    var query = { "email" : req.body.email };
    User.findOne(query)
    .then((data)=>{
        console.log(data);
        let amount =data.maintainance+req.body.maintainance;
        console.log(amount);
        var user = new User();
        user.flat_no = data.flat_no;
        user.email = req.body.email;
        user.maintainance = amount;
        user.reason = req.body.reason;
        user.last_date_of_payment = data.last_date_of_payment;
        console.log(user);
        user.save((err)=>{   
            if(err)
            {
                res.json("error in inserting record"+err);
            }
            else{
                res.json("maintainance is updated and it is now..."+user.maintainance);
            }
        });
       
     })
    .catch((err)=>{
      res.json("no valid data "+err);
    })
   
}
module.exports ={ personal_maintainance_schema,addMaintainance}
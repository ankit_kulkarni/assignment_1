require('../societyprofilerequest/db');
const MONGOOSE = require('mongoose');
const EXPRESS = require('express');
const BODYPARSER = require('body-parser');
var APP = EXPRESS();

const SCHEMA = MONGOOSE.Schema;
let profileschema =new SCHEMA({
    fullname : {type: String ,required : true,maxlength : 100},
    birthdate : {type : Date,required : true},
    sex:{type :String,required : true,maxlength : 5},
    mariatalStatus : {type : String,required :true,maxlength : 9},
    Email : {type : String,required : true},
    flatno:{type : Number,required : true,maxlength : 4},
    noOfFamilyMembers: {type : Number,required : true},
    mobile:{type : Number,required : true,maxlength : 10},
    job:{type : String,required : true,},
    reqPersonName:{type : String,required :true},
    isapproved : {type : Boolean,default :false}
}); 

profileschema.path('Email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
},'Invalid e-mail.');

var User = MONGOOSE.model('profile',profileschema,'profile');
APP.use(BODYPARSER.urlencoded({
    extended :true
}));

APP.use(BODYPARSER.json());

function insertrecord(req,res){
  var user = new User();
  user.fullname = req.body.fullname;
  user.birthdate = req.body.birthdate;
  user.sex = req.body.fullname;
  user.mariatalStatus = req.body.mariatalStatus;
  user.Email = req.body.Email;
  user.flatno = req.body.flatno;
  user.noOfFamilyMembers = req.body.noOfFamilyMembers;
  user.mobile = req.body.mobile;
  user.job = req.body.job;
  user.reqPersonName = req.body.reqPersonName;
  user.isapproved = req.body.isapproved;
   user.save((err,result)=>{   //actually used save()
       if(err)
       {
           res.json(err);
       }
       else{
            res.json("profile successfuly given to accept request");
       }
   });
}
module.exports = {User,insertrecord}